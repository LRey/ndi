// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebase : {
    apiKey: 'AIzaSyAUXMWRA5rlV0Zi77sy5ojukeEPXhC97mA',
    authDomain: 'ndi-2018.firebaseapp.com',
    databaseURL: 'https://ndi-2018.firebaseio.com',
    projectId: 'ndi-2018',
    storageBucket: 'ndi-2018.appspot.com',
    messagingSenderId: '492684355488'
  }
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
