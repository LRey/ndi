import { Component } from '@angular/core';
import { AngularFirestore } from "@angular/fire/firestore";
import { Observable } from 'rxjs';
import {HttpClient, HttpHeaders} from "@angular/common/http"
@Component({
  selector: 'app-root',
  templateUrl: './app.index.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  meteo = {
    "windspeed": 10
  };
  todos: any[];
  todoSelected: any;

  setDetails(todo){
    this.todoSelected = todo;
  }

  

  constructor(private db : AngularFirestore,
              private http: HttpClient){
    db.collection('todos', ref => 
      ref.where("checked", "==", false)
    ).valueChanges().subscribe(data =>{ 
      this.todos = data;  
      console.log(this.todos); } 
    );

    } 

  ngOnInit() {

  }
}

