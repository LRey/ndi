import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  jours: Observable<any[]>;

  constructor(public db: AngularFirestore) {
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit(): void {
    this.jours = this.db.collection('jdb').valueChanges();
  }
}
