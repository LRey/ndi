import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { ExpeditionComponent } from './components/expedition/expedition.component';
import { JdbComponent } from './components/jdb/jdb.component';
import { TodoComponent } from './components/todo/todo.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TodoItemComponent } from './components/todo/todo-item/todo-item.component';



@NgModule({
  declarations: [
    AppComponent,
    ExpeditionComponent,
    JdbComponent,
    TodoComponent,
    DashboardComponent,
    TodoItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    FormsModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
