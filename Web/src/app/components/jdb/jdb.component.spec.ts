import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JdbComponent } from './jdb.component';

describe('JdbComponent', () => {
  let component: JdbComponent;
  let fixture: ComponentFixture<JdbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JdbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JdbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
