import { Time } from '@angular/common';

export interface Item {
  id: string;
  checked: boolean;
  description: string;
  heure: Time;
  details: string;
}
