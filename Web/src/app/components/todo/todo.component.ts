import { Component, OnInit } from '@angular/core';
import { Observable, } from 'rxjs';
import { map } from 'rxjs/operators';

import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { Item } from './item.model';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {

  items: Observable<Item[]>;

  constructor(
    public db: AngularFirestore,
  ) { }

  ngOnInit() {
    this.fetchItems();
  }

  public itemChecked(item: Item) {

    const docRef = this.db.collection('todos').doc(item.id).ref;

    return this.db.firestore.runTransaction(transaction => {
      return transaction.get(docRef).then(doc => {
        if (!doc.exists) {
          throw "Document does not exist!";
        }
        transaction.update(docRef, { checked: item.checked })
      }
      )
    }
    )

  }

  private fetchItems(): void {

    let itemCollection: AngularFirestoreCollection = this.db.collection('todos');
    this.items = itemCollection.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Item;
        const id = a.payload.doc.id;
        return { id, ...data };
      });
    }));

    // this.items = this.db.collection<Item>('todos').valueChanges();
  }


}
