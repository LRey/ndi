# Manuel d'utilisation

## L'équipe
Notre équipe est composée de 11 membres répartis en 3 pôles principaux:
Une partie développement Frontend composée de Léo Jan, Léni Gauffier, Enzo Molion et Lucas Reygrobellet.
Une équipe de développement Backend qui se compose de Maxence Brès (de Lozère), Benjamin Besnier, Joachim Fontfreyde, Timothée Depriester.
Eva Bardou et Camille Picca se sont occupés de la communication et de la création de contenu.
William Weill est le chef d'équipe, il a coordonné les différentes personnes.

## Checklist
Pour y avoir acces, se connecter à l'url : **http://51.68.114.249/**   
Lorsque l'explorateur est dans sa base, il a accès à différents services :   
* Une ToDo liste : Une liste de chose à faire dans la journée   
* Un tableau contenant diverses informations : température, nutriments, météo   
* Une possibilité d'ajouter et de lire son journal de bord (de manière textuelle, audio ou vidéo)   
* Et enfin, il peut indiquer qu'il part pour une expedition, ce qui permettra d'afficher, en fonction de la durée de la sortie, la liste du matériel qui est opérationnel, et le matériel qui nécessite d'etre rechargé, réparé ...  

## Cockpit numérique portable
Pour y avoir acces, se connecter à l'url : **http://51.68.114.249/cockpit**
C'est une interface, idéalement accessible depuis un portable ou une tablette, afin de pouvoir l'emmener lors d'une expedition. C'est une version allégée de la checklist.                     
    
    
## OpenData

Nous avons utilisé, au cours du développement des fonctionnalités, des appels à des API OpenData.
Ainsi OpenWeatherMap nous a fourni les données métérologiques et USGS Earthquake nous a donné les informations sur les séismes environnants.

## Defi Respect de la vie privée

Voir le fichier EIVP_Namib2.pdf

## Vidéo

La vidéo de nos exploits est: https://drive.google.com/open?id=1IiJ4LIi8w-Xo38r7bhFY6tFwIQrARujZ