var express = require('express');
var url = require('url');
var querystring = require('querystring');
var request = require('request');
var gpsDistance = require('gps-distance');
var router = express.Router();
var GPS = require('gps');
var gps = new GPS;
var angles = require('angles');

var serviceAccount = require("../privateKey.json");
var admin = require('firebase-admin');


// constante

var latBase = -21.3387148;
var lonBase=-2.5878429;
var equipement = ['corde','pioche','lampe-torche','décapsuleur','piolet'
                              ,'crampon','bouffadou','marteau','pelle','tente','ordinateur','casque','dorsale'];
var speed = 4;
var batterieHour = 10;
var eauHour = 1;
var nourritureHour = 400; // en kcal

//firebase

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://ndi-2018.firebaseio.com"
});

var db = admin.firestore()

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendFile(__dirname+"/Web/index.html");
});

router.get('/tav/statehistory/:nbr', function(req, res, next) {
  var nbr  = req.params.nbr;
  var result = [];

  db.collection('medical')
  .orderBy("cardio.time", "desc")
  .limit(Number(nbr))
  .get()
  .then((snapshot) => {
    snapshot.forEach((doc) => {
      var cal = doc.data().calorie;
      var time = doc.data().cardio.time;
      var bpm = doc.data().cardio.value;
      var glu = doc.data().glucide;
      var prot = doc.data().proteine;
      var temp = doc.data().temperature;
      var vit = doc.data().vitamine;
      
      result.push([time, {"Rythme cardiaque" : bpm , "Etat":checkBpm(bpm), "Calorie" : checkCalorie(cal), "Proteine" : checkProteine(prot),"Glucide" : checkGlucide(glu), "Vitamines" : checkVitamies(vit), "Temperature corporel" : checkTemp(temp), "Etat de santé global" : checkGeneral(cal,bpm,glu,prot,temp,vit)}]);
      
    });
    res.json(result);
  })
  .catch((err) => {
    console.log('Error getting documents', err);
  });
});

var result = [];
function checkCalorie(cal){
  if(cal < 2000) {
    return ["Sous nutrition",false];
  } else if (cal > 3500) {
    return ["Sur nutrition",false];
  } else {
    return ["Bonne nutrition",true];
  }
}

function checkProteine(prot){
  if(prot < 50) {
    return ["Manque de proteine",false];
  } else if( prot > 60){
    return  ["Surplus de proteine",false];
  } else {
    return ["Apport correct",true];
  }
}

function checkGlucide(glu){
  if(glu < 250) {
    return ["Manque de glucide",false];
  } else if( glu > 500){
    return ["Surplus de glucide",false];
  } else {
    return ["Apport correct",true];
  }
}

function checkVitamies(vit){
  if(vit < 75) {
    return ["Manque de vitamines",false];
  } else if( vit > 120){
    return ["Surplus de vitamines",false];
  } else {
    return ["Apport correct",true];
  }
}

function checkTemp(temp){
  if(temp < 36) {
    return ["Risque d'hypothermie",false];
  } else if( temp > 38){
    return ["Fièvre",false];
  } else {
    return ["R.A.S",true];
  }
}

function checkBpm(bpm) {
  if(bpm < 70) {
    return ["Sous-tention",false];
  } else if( bpm > 140){
    return ["Sur-tension",false];
  } else {
    return ["Normal",true];
  }
}

function checkGeneral(cal,bpm,glu,prot,temp,vit){
  if (checkCalorie(cal)[1] && checkBpm(bpm)[1] && checkGlucide(glu)[1] && checkProteine(prot)[1] && checkTemp(temp)[1] && checkVitamies(vit)[1]) {
    return "Bon";
  } else {
    return "Mauvais";
  }
}



router.get('/tav/currentState', function(req, res, next) {
  
  db.collection('medical').orderBy('cardio.time','desc').limit(1).get()
  .then((snapshot) => {
    snapshot.forEach((doc) =>{
      var cal = doc.data().calorie;
      var time = doc.data().cardio.time;
      var bpm = doc.data().cardio.value;
      var glu = doc.data().glucide;
      var prot = doc.data().proteine;
      var temp = doc.data().temperature;
      var vit = doc.data().vitamine;

      result = [time, {"Rythme cardiaque" : bpm , "Etat":checkBpm(bpm)[0], "Calorie" : checkCalorie(cal)[0], "Proteine" : checkProteine(prot)[0],"Glucide" : checkGlucide(glu)[0], "Vitamines" : checkVitamies(vit)[0], "Temperature corporel" : checkTemp(temp)[0], "Etat de santé global" : checkGeneral(cal,bpm,glu,prot,temp,vit)}];
      res.json(result);

    })
  })
  .catch((err) => {
    console.log('Error getting documents', err);
  });  
});

router.get('/weather/current', function (req, res, next) {
  var params = querystring.parse(url.parse(req.url).query);
  console.log(params);
  request('https://api.openweathermap.org/data/2.5/weather?lat=' + params['lat'] + '&lon=' + params['lon']
    + '&APPID=9537da29d5644067877490569c3ad749', function (error, response, body) {
      res.write(body);
      res.end();
    });
});

router.get('/weather/future', function (req, res, next) {
  var params = querystring.parse(url.parse(req.url).query);
  console.log(params);
  request('https://api.openweathermap.org/data/2.5/forecast?lat=' + params['lat'] + '&lon=' + params['lon']
    + '&APPID=9537da29d5644067877490569c3ad749', function (error, response, body) {
      res.write(body);
      res.end();
    });
});

router.get('/seisme/today', function (req, res, next) {
  var params = querystring.parse(url.parse(req.url).query);
  var date = new Date(Date.now());
  date.setHours(0);
  date.setMinutes(0);

  request('https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=' + date.toUTCString() + '&minmagnitude=5&latitude=' + params['lat'] + '&longitude=' + params['lon'] + '&maxradiuskm=500',
    function (error, response, body) {
      res.write(body);
      res.end();
    });
});

router.get('/tav/currentState', function (req, res, next) {

  db.collection('medical').limit(1).get()
    .then((snapshot) => {
      snapshot.forEach((doc) => {
        res.json(doc.data().cardio.value);
      })
    })
    .catch((err) => {
      console.log('Error getting documents', err);
    });
  });

router.get('/depart_mission', function (req, res,next) {

  var params = querystring.parse(url.parse(req.url).query);

  var arriveeLon = parseFloat(params['lon']);
  var arriveeLat = parseFloat(params['lat']);
  //var heure = parseFloat(params['heure']);
  var dist = gpsDistance(latBase,lonBase,arriveeLat,arriveeLon);
  //var dist = gps(12,12,14,14);
  var heure = Math.round((new Date()).getTime() / 1000);
  var duree = ((dist / speed) * 60 * 60);
  var heureArrivee = heure + duree;
  var heureRetour = heureArrivee + duree;
  //console.log(dist + ' ' + heureArrivee + ' ' + heureRetour );
  var equipement1 = equipement[parseInt(Math.random() * equipement.length)];
  var equipement2 = equipement[parseInt(Math.random() * equipement.length)];
  var equipement3 = equipement[parseInt(Math.random() * equipement.length)];
  var tabEquip = [equipement1,equipement2,equipement3];

  var batterie = (duree / 60 /60 ) * batterieHour;
  var eau = (duree / 60 / 60) * eauHour;
  var nourriture = (duree / 60 / 60)  * nourritureHour;

  result = [ {"distance":dist, "heureArrivee":heureArrivee, "heureRetour":heureRetour,"batterie":batterie,"eau":eau,"nourriture":nourriture,"tabEquip":tabEquip }];
  res.json(result);
});

router.get('/cockpit_gps', function (req, res, next) {

  var params = querystring.parse(url.parse(req.url).query);

  var lonD = parseFloat(params['lonD']);
  var latD = parseFloat(params['latD']);
  var lonA = parseFloat(params['lonA']);
  var latA = parseFloat(params['latA']);
  var dist = gpsDistance(latD, lonD, latA, lonA);
  var duree = ((dist / speed) * 60 * 60);
  orientation = angles.compass(GPS.Heading(latD, lonD, latA, lonA));
  result = [{ "distance": dist, "duree": duree,"orientation":orientation}];
  res.json(result);
});


module.exports = router;

