$.noConflict();

jQuery(document).ready(function ($) {

	"use strict";

	[].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
		new SelectFx(el);
	});

	jQuery('.selectpicker').selectpicker;




	$('.search-trigger').on('click', function (event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').addClass('open');
	});

	$('.search-close').on('click', function (event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').removeClass('open');
	});

	// $('.equal-height').matchHeight({
	// 	property: 'max-height'
	// });

	// var chartsheight = $('.flotRealtime2').height();
	// $('.traffic-chart').css('height', chartsheight-122);


	// Counter Number
	$('.count').each(function () {
		$(this).prop('Counter', 0).animate({
			Counter: $(this).text()
		}, {
				duration: 3000,
				easing: 'swing',
				step: function (now) {
					$(this).text(Math.ceil(now));
				}
			});
	});




	// Menu Trigger
	$('#menuToggle').on('click', function (event) {
		var windowWidth = $(window).width();
		if(windowWidth < 1010) {
			$('body').removeClass('open');
			if(windowWidth < 760) {
				$('#left-panel').slideToggle();
			} else {
				$('#left-panel').toggleClass('open-menu');
			}
		} else {
			$('body').toggleClass('open');
			$('#left-panel').removeClass('open-menu');
		}

	});


	$(".menu-item-has-children.dropdown").each(function () {
		$(this).on('click', function () {
			var $temp_text = $(this).children('.dropdown-toggle').html();
			$(this).children('.sub-menu').prepend('<li class="subtitle">' + $temp_text + '</li>');
		});
	});


	// Load Resize
	$(window).on("load resize", function (event) {
		var windowWidth = $(window).width();
		if(windowWidth < 1010) {
			$('body').addClass('small-device');
		} else {
			$('body').removeClass('small-device');
		}

	});


});
window.onload = function () {




	var svg = null;
	var circle = null;
	var circleTransition = null;
	var latestBeat = null;
	var insideBeat = false;
	var data = [];

	var SECONDS_SAMPLE = 5;
	var BEAT_TIME = 400;
	var TICK_FREQUENCY = SECONDS_SAMPLE * 1000 / BEAT_TIME;
	var BEAT_VALUES = [0, 0, 3, -4, 10, -7, 3, 0, 0];

	var CIRCLE_FULL_RADIUS = 40;
	var MAX_LATENCY = 5000;

	var colorScale = d3.scale.linear()
		.domain([BEAT_TIME, (MAX_LATENCY - BEAT_TIME) / 2, MAX_LATENCY])
		.range(["#6D9521", "#D77900", "#CD3333"]);

	var radiusScale = d3.scale.linear()
		.range([5, CIRCLE_FULL_RADIUS])
		.domain([MAX_LATENCY, BEAT_TIME]);

	function beat() {

		if(insideBeat) return;
		insideBeat = true;

		var now = new Date();
		var nowTime = now.getTime();

		if(data.length > 0 && data[data.length - 1].date > now) {
			data.splice(data.length - 1, 1);
		}

		data.push({
			date: now,
			value: 0
		});

		var step = BEAT_TIME / BEAT_VALUES.length - 2;
		for(var i = 1; i < BEAT_VALUES.length; i++) {
			data.push({
				date: new Date(nowTime + i * step),
				value: BEAT_VALUES[i]
			});
		}

		latestBeat = now;

		circleTransition = circle.transition()
			.duration(BEAT_TIME)
			.attr("r", CIRCLE_FULL_RADIUS)
			.attr("fill", "#6D9521");

		setTimeout(function () {
			insideBeat = false;
		}, BEAT_TIME);
	}

	var svgWrapper = document.getElementById("svg-wrapper");
	var margin = { left: 10, top: 10, right: CIRCLE_FULL_RADIUS * 3, bottom: 10 },
		width = svgWrapper.offsetWidth - margin.left - margin.right,
		height = svgWrapper.offsetHeight - margin.top - margin.bottom;

	// create SVG
	svg = d3.select('#svg-wrapper').append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.bottom + margin.top)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	circle = svg
		.append("circle")
		.attr("fill", "#6D9521")
		.attr("cx", width + margin.right / 2)
		.attr("cy", height / 2)
		.attr("r", CIRCLE_FULL_RADIUS);

	// init scales
	var now = new Date(),
		fromDate = new Date(now.getTime() - SECONDS_SAMPLE * 1000);

	// create initial set of data
	data.push({
		date: now,
		value: 0
	});

	var x = d3.time.scale()
		.domain([fromDate, new Date(now.getTime())])
		.range([0, width]),
		y = d3.scale.linear()
			.domain([-10, 10])
			.range([height, 0]);

	var line = d3.svg.line()
		.interpolate("basis")
		.x(function (d) {
			return x(d.date);
		})
		.y(function (d) {
			return y(d.value);
		});

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom")
		.ticks(d3.time.seconds, 1)
		.tickFormat(function (d) {
			var seconds = d.getSeconds() === 0 ? "00" : d.getSeconds();
			return seconds % 10 === 0 ? d.getMinutes() + ":" + seconds : ":" + seconds;
		});

	// add clipPath
	svg.append("defs").append("clipPath")
		.attr("id", "clip")
		.append("rect")
		.attr("width", width)
		.attr("height", height);

	var axis = d3.select("svg").append("g")
		.attr("class", "axis")
		.attr("transform", "translate(0," + height + ")")
		.call(xAxis);

	var path = svg.append("g")
		.attr("clip-path", "url(#clip)")
		.append("path")
		.attr("class", "line");

	svg.select(".line")
		.attr("d", line(data));

	var transition = d3.select("path").transition()
		.duration(100)
		.ease("linear");

	(function tick() {

		transition = transition.each(function () {

			// update the domains
			now = new Date();
			fromDate = new Date(now.getTime() - SECONDS_SAMPLE * 1000);
			x.domain([fromDate, new Date(now.getTime() - 100)]);

			var translateTo = x(new Date(fromDate.getTime()) - 100);

			// redraw the line
			svg.select(".line")
				.attr("d", line(data))
				.attr("transform", null)
				.transition()
				.attr("transform", "translate(" + translateTo + ")");

			// slide the x-axis left
			axis.call(xAxis);

		}).transition().each("start", tick);
	})();

	setInterval(function () {

		now = new Date();
		fromDate = new Date(now.getTime() - SECONDS_SAMPLE * 1000);

		for(var i = 0; i < data.length; i++) {
			if(data[i].date < fromDate) {
				data.shift();
			} else {
				break;
			}
		}

		if(insideBeat) return;

		data.push({
			date: now,
			value: 0
		});

		if(circleTransition != null) {

			var diff = now.getTime() - latestBeat.getTime();

			if(diff < MAX_LATENCY) {
				circleTransition = circle.transition()
					.duration(TICK_FREQUENCY)
					.attr("r", radiusScale(diff))
					.attr("fill", colorScale(diff));
			}
		}


	}, TICK_FREQUENCY);

	setInterval(function () {
		beat();
	}, 2000);
	beat();
};
