(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_expedition_new_expedition_new_expedition_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/expedition/new-expedition/new-expedition.component */ "./src/app/components/expedition/new-expedition/new-expedition.component.ts");
/* harmony import */ var _components_home_home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/home/home/home.component */ "./src/app/components/home/home/home.component.ts");





var routes = [
    {
        path: 'newExpedition', component: _components_expedition_new_expedition_new_expedition_component__WEBPACK_IMPORTED_MODULE_3__["NewExpeditionComponent"]
    },
    {
        path: '', component: _components_home_home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"]
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: currentDay, AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "currentDay", function() { return currentDay; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var currentDay = 9;
var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent.prototype.ngOnInit = function () {
        this.currentDay = 9;
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _components_expedition_expedition_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/expedition/expedition.component */ "./src/app/components/expedition/expedition.component.ts");
/* harmony import */ var _components_jdb_jdb_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/jdb/jdb.component */ "./src/app/components/jdb/jdb.component.ts");
/* harmony import */ var _components_todo_todo_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/todo/todo.component */ "./src/app/components/todo/todo.component.ts");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/dashboard/dashboard.component */ "./src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var _safe_html_pipe__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./safe-html.pipe */ "./src/app/safe-html.pipe.ts");
/* harmony import */ var _components_expedition_new_expedition_new_expedition_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/expedition/new-expedition/new-expedition.component */ "./src/app/components/expedition/new-expedition/new-expedition.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _components_home_home_home_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/home/home/home.component */ "./src/app/components/home/home/home.component.ts");
/* harmony import */ var _components_weather_weather_weather_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/weather/weather/weather.component */ "./src/app/components/weather/weather/weather.component.ts");



















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _components_expedition_expedition_component__WEBPACK_IMPORTED_MODULE_10__["ExpeditionComponent"],
                _components_jdb_jdb_component__WEBPACK_IMPORTED_MODULE_11__["JdbComponent"],
                _components_todo_todo_component__WEBPACK_IMPORTED_MODULE_12__["TodoComponent"],
                _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_13__["DashboardComponent"],
                _safe_html_pipe__WEBPACK_IMPORTED_MODULE_14__["SafeHtmlPipe"],
                _components_expedition_new_expedition_new_expedition_component__WEBPACK_IMPORTED_MODULE_15__["NewExpeditionComponent"],
                _components_home_home_home_component__WEBPACK_IMPORTED_MODULE_17__["HomeComponent"],
                _components_weather_weather_weather_component__WEBPACK_IMPORTED_MODULE_18__["WeatherComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_fire__WEBPACK_IMPORTED_MODULE_7__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].firebase),
                _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_8__["AngularFirestoreModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_16__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n  <div class=\"card-header\">\n    <div class=\"stat-widget-five\">\n      <div class=\"stat-icon dib flat-color-1\">\n        <i class=\"fas fa-heartbeat\"></i>\n      </div>\n      <div class=\"stat-content\">\n        <div class=\"text-left dib\">\n          <div class=\"stat-text\"><span class=\"\">HEALTH</span></div>\n\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"card-body\">\n    <div class=\"row\">\n      <div class=\"col-lg-8\" style=\" position: relative; \">\n        <div id=\"svg-wrapper\" style=\" width: 100%; \"></div>\n      </div>\n      <div class=\"col-lg-4\">\n        <div class=\"card-body\">\n          <div class=\"progress-box progress-1\">\n            <h4 class=\"por-title\">Calories</h4>\n            <div class=\"por-txt\">96,930 KCal (40%)</div>\n            <div class=\"progress mb-2\" style=\"height: 5px;\">\n              <div class=\"progress-bar bg-flat-color-1\" role=\"progressbar\" style=\"width: 40%;\" aria-valuenow=\"25\"\n                aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n            </div>\n          </div>\n          <div class=\"progress-box progress-2\">\n            <h4 class=\"por-title\">Proteines</h4>\n            <div class=\"por-txt\">3,220 mg (24%)</div>\n            <div class=\"progress mb-2\" style=\"height: 5px;\">\n              <div class=\"progress-bar bg-flat-color-2\" role=\"progressbar\" style=\"width: 24%;\" aria-valuenow=\"25\"\n                aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n            </div>\n          </div>\n          <div class=\"progress-box progress-2\">\n            <h4 class=\"por-title\">Glucides</h4>\n            <div class=\"por-txt\">29,658 mg (60%)</div>\n            <div class=\"progress mb-2\" style=\"height: 5px;\">\n              <div class=\"progress-bar bg-flat-color-3\" role=\"progressbar\" style=\"width: 60%;\" aria-valuenow=\"60\"\n                aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n            </div>\n          </div>\n          <div class=\"progress-box progress-2\">\n            <h4 class=\"por-title\">Vitamines</h4>\n            <div class=\"por-txt\">99,658 (90%)</div>\n            <div class=\"progress mb-2\" style=\"height: 5px;\">\n              <div class=\"progress-bar bg-flat-color-4\" role=\"progressbar\" style=\"width: 90%;\" aria-valuenow=\"90\"\n                aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n            </div>\n          </div>\n        </div> <!-- /.card-body -->\n      </div>\n\n    </div>\n    <div class=\"row\">\n      <div class=\"col-lg-12\">\n        <div class=\"\" style=\" margin: 0 45px 30px; \">\n          <h2>Température : 37°</h2>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.ts ***!
  \*************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DashboardComponent = /** @class */ (function () {
    function DashboardComponent() {
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/components/dashboard/dashboard.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/components/expedition/expedition.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/expedition/expedition.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n  <div class=\"card-header\">\n    <div class=\"stat-widget-five\">\n      <div class=\"stat-content\" style=\" width: 100%; text-align: center; margin: 0; \">\n        <div class=\"text-left dib\">\n          <div class=\"stat-text\"><span class=\"\">PARTIR EN EXPEDITION</span></div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"card-body\">\n    <div class=\"col-lg-12\">\n      <button (click)=\"gotoExpedition()\" type=\"button\" class=\"btn btn-success\" style=\" width: 100%; height: 70px; \"><i\n          class=\"fa fa-walking\"></i><a style=\"color:white\"> START</a></button>\n    </div>\n  </div>\n</div>\n\n<!-- <button type=\"button\" class=\"btn btn-primary\" (click)=\"gotoExpedition()\">Expedition</button> -->\n"

/***/ }),

/***/ "./src/app/components/expedition/expedition.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/components/expedition/expedition.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZXhwZWRpdGlvbi9leHBlZGl0aW9uLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/expedition/expedition.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/expedition/expedition.component.ts ***!
  \***************************************************************/
/*! exports provided: ExpeditionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpeditionComponent", function() { return ExpeditionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var ExpeditionComponent = /** @class */ (function () {
    function ExpeditionComponent(router) {
        this.router = router;
    }
    ExpeditionComponent.prototype.ngOnInit = function () {
    };
    ExpeditionComponent.prototype.gotoExpedition = function () {
        this.router.navigateByUrl('/newExpedition');
    };
    ExpeditionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-expedition',
            template: __webpack_require__(/*! ./expedition.component.html */ "./src/app/components/expedition/expedition.component.html"),
            styles: [__webpack_require__(/*! ./expedition.component.scss */ "./src/app/components/expedition/expedition.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ExpeditionComponent);
    return ExpeditionComponent;
}());



/***/ }),

/***/ "./src/app/components/expedition/new-expedition/new-expedition.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/components/expedition/new-expedition/new-expedition.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"right-panel\" class=\"right-panel\">\n  <header id=\"header\" class=\"header\">\n    <div class=\"top-left\">\n      <i class=\"fas fa-home fa-2x\" style=\"margin: 10px 0px;\"></i>\n    </div>\n    <div class=\"header-menu\" style=\" position: absolute; left: 45%; \">\n      <h2 style=\" line-height: 50px; margin: auto;\">EXPEDITION</h2>\n    </div>\n  </header>\n  <!-- Content -->\n  <div class=\"content\">\n    <!-- Animated -->\n    <div class=\"animated fadeIn\">\n      <!-- Widgets  -->\n      <div class=\"row\">\n        <div class=\"col-lg-6 col-md-6 col-sm-6\">\n          <div class=\"card\">\n            <div class=\"card-body\">\n              <h2><i class=\"fas fa-walking\"></i> Distance :</h2>\n              <br>\n              <h3>32.4km</h3>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-lg-6 col-md-6 col-sm-6\">\n          <div class=\"card\">\n            <div class=\"card-body\">\n              <h2><i class=\"fas fa-stopwatch\"></i> Temps de l'expedition :</h2>\n              <br>\n              <h3>318 min</h3>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-lg-6 col-md-6 col-sm-6\">\n          <div class=\"card\">\n            <div class=\"card-body\">\n              <h2><i class=\"fas fa-clock\"></i> Heure d'arrivée :</h2>\n              <br>\n              <h3>13h21</h3>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-lg-6 col-md-6 col-sm-6\">\n          <div class=\"card\">\n            <div class=\"card-body\">\n              <h2><i class=\"far fa-clock\"></i> Heure de retour :</h2>\n              <br>\n              <h3>16h39</h3>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-lg-12 col-md-12 col-sm-12\">\n          <div class=\"card\">\n            <div class=\"card-header\">\n              <div class=\"stat-widget-five\">\n                <div class=\"stat-icon dib flat-color-5\">\n                  <i class=\"fas fa-toolbox\"></i>\n                </div>\n                <div class=\"stat-content\">\n                  <div class=\"text-left dib\">\n                    <div class=\"stat-text\"><span class=\"\">MATERIEL NECESSAIRE</span></div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"card-body\">\n\n              <table class=\"table\">\n                <thead>\n                  <tr>\n                    <th scope=\"col\">Check</th>\n                    <th scope=\"col\">Materiel</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr>\n                    <th scope=\"row\"><input type=\"checkbox\" class=\"switch-input\" checked=\"true\"></th>\n                    <td>Une pelle</td>\n                  </tr>\n\n                  <tr>\n                    <th scope=\"row\"><input type=\"checkbox\" class=\"switch-input\" checked=\"true\"></th>\n                    <td>40% de batterie sur le Cockpit</td>\n                  </tr>\n                  <tr>\n                    <th scope=\"row\"><input type=\"checkbox\" class=\"switch-input\" checked=\"true\"></th>\n                    <td>1L d'eau</td>\n                  </tr>\n                </tbody>\n              </table>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <!-- .animated -->\n  </div>\n  <!-- /.content -->\n  <div class=\"clearfix\"></div>\n  <!-- Footer -->\n  <button type=\"button\" class=\"btn btn-primary\" (click)=\"createNewExpedition(50, 50)\">Ask for (lat, lon)= (50,50)</button>\n  <p>\n    Received data : {{ receivedData | json}}\n  </p>\n  <footer class=\"site-footer\">\n    <div class=\"footer-inner bg-white\">\n      <div class=\"row\">\n        <div class=\"col-sm-6\">\n          Copyright &copy; 2018 Equipe 2\n        </div>\n        <div class=\"col-sm-6 text-right\">\n          Designed by Lucas Reygrobellet\n        </div>\n      </div>\n    </div>\n  </footer>\n  <!-- /.site-footer -->\n</div>\n"

/***/ }),

/***/ "./src/app/components/expedition/new-expedition/new-expedition.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/components/expedition/new-expedition/new-expedition.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZXhwZWRpdGlvbi9uZXctZXhwZWRpdGlvbi9uZXctZXhwZWRpdGlvbi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/expedition/new-expedition/new-expedition.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/components/expedition/new-expedition/new-expedition.component.ts ***!
  \**********************************************************************************/
/*! exports provided: NewExpeditionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewExpeditionComponent", function() { return NewExpeditionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var NewExpeditionComponent = /** @class */ (function () {
    function NewExpeditionComponent(http) {
        this.http = http;
    }
    NewExpeditionComponent.prototype.ngOnInit = function () {
    };
    NewExpeditionComponent.prototype.createNewExpedition = function (lat, lon) {
        var _this = this;
        // /depart_mission?lat=xxx&lon=xxx
        var url = "/depart_mission?lat=" + lat + "&lon=" + lon;
        console.log(url);
        this.http.get(url).subscribe(function (res) {
            _this.receivedData = res;
        });
    };
    NewExpeditionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-expedition',
            template: __webpack_require__(/*! ./new-expedition.component.html */ "./src/app/components/expedition/new-expedition/new-expedition.component.html"),
            styles: [__webpack_require__(/*! ./new-expedition.component.scss */ "./src/app/components/expedition/new-expedition/new-expedition.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], NewExpeditionComponent);
    return NewExpeditionComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home/home.component.html":
/*!**********************************************************!*\
  !*** ./src/app/components/home/home/home.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"right-panel\" class=\"right-panel\">\n  <header id=\"header\" class=\"header\">\n    <div class=\"header-menu\">\n      <h2 style=\" line-height: 50px; margin: auto;\">DASHBOARD</h2>\n    </div>\n  </header>\n  <!-- Content -->\n  <div class=\"content\">\n    <!-- Animated -->\n    <div class=\"animated fadeIn\">\n      <div class=\"row\">\n        <div class=\"col-lg-6 col-md-6 col-sm-6\">\n          <app-dashboard></app-dashboard>\n          <app-jdb></app-jdb>\n        </div>\n        <div class=\"col-lg-6 col-md-6 col-sm-6\">\n          <app-todo></app-todo>\n          <app-expedition></app-expedition>\n          <app-weather></app-weather>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/home/home/home.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/components/home/home/home.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaG9tZS9ob21lL2hvbWUuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/home/home/home.component.ts":
/*!********************************************************!*\
  !*** ./src/app/components/home/home/home.component.ts ***!
  \********************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/components/home/home/home.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/jdb/jdb.component.html":
/*!***************************************************!*\
  !*** ./src/app/components/jdb/jdb.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n    <div class=\"card-header\">\n        <div class=\"stat-widget-five\">\n            <div class=\"stat-icon dib flat-color-4\">\n                <i class=\"fas fa-book-open\"></i>\n            </div>\n            <div class=\"stat-content\">\n                <div class=\"text-left dib\">\n                    <div class=\"stat-text\"><span class=\"\">JOURNAL DE BORD</span></div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-lg-6\" style=\" border-right: solid 1px gainsboro; \">\n            <button (click)=\"openAdd(add)\" style=\" width: 90%; height: 60px; margin: 1.2em; border: none; background: none\">\n                <i class=\"fas fa-edit fa-3x\"></i>\n            </button>\n        </div>\n        <div class=\"col-lg-6\">\n            <button (click)=\"openList(list)\" style=\" width: 90%; height: 60px; margin: 1.25em; border: none; background: none\">\n                <i class=\"fas fa-list-ul fa-3x\"></i>\n            </button>\n        </div>\n    </div>\n</div>\n\n\n<ng-template #list let-modal>\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\" id=\"modal-basic-title\">Jour {{selectedDay}}</h4>\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"modal.dismiss()\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n    <div class=\"modal-body\">\n        <div *ngFor=\"let message of messagesList\" class=\"card\">\n            <div *ngIf=\"message.type == 'text'\">\n                {{message.value}}\n            </div>\n            <div *ngIf=\"message.type == 'video'\">\n                <iframe [src]=\"message.value | safeHtml\" frameborder=\"0\" allowfullscreen></iframe>\n            </div>\n        </div>\n    </div>\n    <div class=\"modal-footer\">\n        <button class=\"btn btn-lg\" (click)=\"changeDay(-1)\">Précédent</button>\n        <button class=\"btn btn-lg\" (click)=\"changeDay(1)\">Suivant</button>\n        <div ngbDropdown class=\"d-inline-block\">\n            <button class=\"btn btn-outline-primary\" id=\"dropdownBasic1\" ngbDropdownToggle>Toggle dropdown</button>\n            <div ngbDropdownMenu aria-labelledby=\"dropdownBasic1\">\n                <button *ngFor=\"let day of dayArray\" (click)=\"selectDay(day)\" class=\"dropdown-item\">{{day}}</button>\n            </div>\n        </div>\n    </div>\n</ng-template>\n\n<ng-template #add let-modal>\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\" id=\"modal-basic-title\">Ajouter une note</h4>\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"modal.dismiss()\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n    <div class=\"modal-body\">\n        <div class=\"form-group\">\n            <div class=\"input-group\">\n                <textarea class=\"form-control\" rows=\"5\" [(ngModel)]=\"addText\"></textarea>\n            </div>\n        </div>\n    </div>\n    <div class=\"modal-footer\">\n        <button class=\"btn btn-success\" (click)=\"modal.close(addText)\">\n            Sauver\n        </button>\n    </div>\n</ng-template>\n"

/***/ }),

/***/ "./src/app/components/jdb/jdb.component.scss":
/*!***************************************************!*\
  !*** ./src/app/components/jdb/jdb.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvamRiL2pkYi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/jdb/jdb.component.ts":
/*!*************************************************!*\
  !*** ./src/app/components/jdb/jdb.component.ts ***!
  \*************************************************/
/*! exports provided: JdbComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JdbComponent", function() { return JdbComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../app.component */ "./src/app/app.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");






var JdbComponent = /** @class */ (function () {
    function JdbComponent(db, modalService) {
        this.db = db;
        this.modalService = modalService;
    }
    JdbComponent.prototype.ngOnInit = function () {
        this.jdb = this.db.collection('jdb');
        this.selectedDay = _app_component__WEBPACK_IMPORTED_MODULE_4__["currentDay"];
        this.dayArray = Array.from(Array(_app_component__WEBPACK_IMPORTED_MODULE_4__["currentDay"] + 1).keys());
        this.messagesObservable = this.jdb.valueChanges();
    };
    JdbComponent.prototype.retrieveMessages = function (d) {
        var _this = this;
        this.db.collection('jdb', function (ref) { return ref.where('day', '==', d).orderBy('time'); }).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1)).subscribe(function (data) {
            _this.messagesList = data;
        });
    };
    JdbComponent.prototype.add = function (tp, value) {
        this.retrieveMessages(_app_component__WEBPACK_IMPORTED_MODULE_4__["currentDay"]);
        var tm = Date.now();
        var newId = this.db.createId();
        this.jdb.doc(newId).set({
            'value': value,
            'type': tp,
            'time': tm,
            'day': _app_component__WEBPACK_IMPORTED_MODULE_4__["currentDay"]
        });
        console.log(this.messagesList);
    };
    JdbComponent.prototype.selectDay = function (i) {
        this.selectedDay = i;
        this.retrieveMessages(this.selectedDay);
    };
    JdbComponent.prototype.changeDay = function (i) {
        if (this.selectedDay > 0 && i < 0) {
            this.selectedDay += i;
        }
        if (this.selectedDay < _app_component__WEBPACK_IMPORTED_MODULE_4__["currentDay"] && i > 0) {
            this.selectedDay += i;
        }
        this.retrieveMessages(this.selectedDay);
    };
    JdbComponent.prototype.openAdd = function (content) {
        var _this = this;
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(function (text) {
            _this.add('text', text);
        }, function () { });
    };
    JdbComponent.prototype.openList = function (content) {
        this.retrieveMessages(_app_component__WEBPACK_IMPORTED_MODULE_4__["currentDay"]);
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(function () {
        }, function () { });
    };
    JdbComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-jdb',
            template: __webpack_require__(/*! ./jdb.component.html */ "./src/app/components/jdb/jdb.component.html"),
            styles: [__webpack_require__(/*! ./jdb.component.scss */ "./src/app/components/jdb/jdb.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"]])
    ], JdbComponent);
    return JdbComponent;
}());



/***/ }),

/***/ "./src/app/components/todo/todo.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/todo/todo.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n  <div class=\"card-header\">\n    <div class=\"stat-widget-five\">\n      <div class=\"stat-icon dib flat-color-3\">\n        <i class=\"far fa-list-alt\"></i>\n      </div>\n      <div class=\"stat-content\">\n        <div class=\"text-left dib\">\n          <div class=\"stat-text\"><span class=\"\">Todo</span></div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n\n  <div class=\"card-body\">\n    <table class=\"table\">\n      <thead>\n        <tr>\n          <th scope=\"col\">Done</th>\n          <th scope=\"col\">Heure</th>\n          <th scope=\"col\">Desc</th>\n          <th scope=\"col\">More</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor=\"let item of items | async\">\n          <th scope=\"row\">\n            <input type=\"checkbox\" class=\"switch-input\" [(ngModel)]=\"item.checked\" (change)=\"itemChecked(item)\"></th>\n          <td>{{item.heure.toDate() | date: 'medium'}}</td>\n          <td>{{item.description}}</td>\n          <td><button data-toogle=\"modal\" attr.data-target=\"#modal{{item.id}}\" type=\"button\" class=\"btn btn-outline-primary\"\n              style=\"\n                                    width: 100%;\n                                    \">Detail</button></td>\n\n          <div class=\"modal fade\" id=\"modal{{item.id}}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\n            aria-hidden=\"true\">\n            <div class=\"modal-dialog\" role=\"document\">\n              <div class=\"modal-content\">\n                <div class=\"modal-header\">\n                  <h5 class=\"modal-title\" id=\"exampleModalLabel\">{{item.description}}</h5>\n                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                    <span aria-hidden=\"true\">&times;</span>\n                  </button>\n                </div>\n                <div class=\"modal-body\">\n                  {{ item.details }}\n                </div>\n                <div class=\"modal-footer\">\n                  <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n                </div>\n              </div>\n            </div>\n          </div>\n        </tr>\n\n      </tbody>\n    </table>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/components/todo/todo.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/components/todo/todo.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdG9kby90b2RvLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/todo/todo.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/todo/todo.component.ts ***!
  \***************************************************/
/*! exports provided: TodoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoComponent", function() { return TodoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");




var TodoComponent = /** @class */ (function () {
    function TodoComponent(db) {
        this.db = db;
    }
    TodoComponent.prototype.ngOnInit = function () {
        this.fetchItems();
    };
    TodoComponent.prototype.itemChecked = function (item) {
        var docRef = this.db.collection('todos').doc(item.id).ref;
        return this.db.firestore.runTransaction(function (transaction) {
            return transaction.get(docRef).then(function (doc) {
                if (!doc.exists) {
                    throw "Document does not exist!";
                }
                transaction.update(docRef, { checked: item.checked });
            });
        });
    };
    TodoComponent.prototype.fetchItems = function () {
        var itemCollection = this.db.collection('todos');
        this.items = itemCollection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (actions) {
            return actions.map(function (a) {
                var data = a.payload.doc.data();
                var id = a.payload.doc.id;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ id: id }, data);
            });
        }));
        // this.items = this.db.collection<Item>('todos').valueChanges();
    };
    TodoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-todo',
            template: __webpack_require__(/*! ./todo.component.html */ "./src/app/components/todo/todo.component.html"),
            styles: [__webpack_require__(/*! ./todo.component.scss */ "./src/app/components/todo/todo.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"]])
    ], TodoComponent);
    return TodoComponent;
}());



/***/ }),

/***/ "./src/app/components/weather/weather/weather.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/weather/weather/weather.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n  <div class=\"card-header\">\n    <div class=\"stat-widget-five\">\n      <div class=\"stat-icon dib flat-color-6\">\n        <i class=\"fas fa-cloud-sun\"></i>\n      </div>\n      <div class=\"stat-content\">\n        <div class=\"text-left dib\">\n          <div class=\"stat-text\"><span class=\"\">METEO</span></div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"card-body\">\n    <div class=\"wrapper\">\n      <div class=\"widget-container\">\n        <div class=\"top-left\">\n          <h2 id=\"day\">Day</h2>\n          <h3 id=\"date\">Month, Day Year</h3>\n          <h3 id=\"time\">Time</h3>\n          <!--       <a target=\"_blank\" href=\"https://codepen.io/myleschuahiock/\"><p id=\"codepen-link\">codepen.io/myleschuahiock</p></a> -->\n          <p class=\"geo\"></p>\n        </div>\n        <div class=\"top-right\">\n          <h1 id=\"weather-status\">Weather / Weather Status</h1>\n          <span>\n            <i class=\"fas fa-sun fa-7x\"></i>\n          </span>\n        </div>\n\n        <div class=\"bottom-left\">\n          <h1 id=\"temperature\">0</h1>\n          <h2 id=\"celsius\">°C</h2>\n          <h2 id=\"temp-divider\">/</h2>\n          <h2 id=\"fahrenheit\">°F</h2>\n        </div>\n\n        <div class=\"bottom-right\">\n          <div class=\"other-details-key\">\n            <p>Wind Speed</p>\n            <p>Humidity</p>\n            <p>Pressure</p>\n            <p>Sunrise Time</p>\n            <p>Sunset Time</p>\n          </div>\n          <div class=\"other-details-values\">\n            <p class=\"windspeed\">0 Km/h</p>\n            <p class=\"humidity\">0 %</p>\n            <p class=\"pressure\">0 hPa</p>\n            <p class=\"sunrise-time\">0:00 am</p>\n            <p class=\"sunset-time\">0:00 pm</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/weather/weather/weather.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/components/weather/weather/weather.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvd2VhdGhlci93ZWF0aGVyL3dlYXRoZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/weather/weather/weather.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/weather/weather/weather.component.ts ***!
  \*****************************************************************/
/*! exports provided: WeatherComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WeatherComponent", function() { return WeatherComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var WeatherComponent = /** @class */ (function () {
    function WeatherComponent() {
    }
    WeatherComponent.prototype.ngOnInit = function () {
    };
    WeatherComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-weather',
            template: __webpack_require__(/*! ./weather.component.html */ "./src/app/components/weather/weather/weather.component.html"),
            styles: [__webpack_require__(/*! ./weather.component.scss */ "./src/app/components/weather/weather/weather.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WeatherComponent);
    return WeatherComponent;
}());



/***/ }),

/***/ "./src/app/safe-html.pipe.ts":
/*!***********************************!*\
  !*** ./src/app/safe-html.pipe.ts ***!
  \***********************************/
/*! exports provided: SafeHtmlPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SafeHtmlPipe", function() { return SafeHtmlPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");



var SafeHtmlPipe = /** @class */ (function () {
    function SafeHtmlPipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    SafeHtmlPipe.prototype.transform = function (content) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(content);
    };
    SafeHtmlPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'safeHtml'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]])
    ], SafeHtmlPipe);
    return SafeHtmlPipe;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyAUXMWRA5rlV0Zi77sy5ojukeEPXhC97mA',
        authDomain: 'ndi-2018.firebaseapp.com',
        databaseURL: 'https://ndi-2018.firebaseio.com',
        projectId: 'ndi-2018',
        storageBucket: 'ndi-2018.appspot.com',
        messagingSenderId: '492684355488'
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/enzo/Documents/RICM5/S9/ndi/Web/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map