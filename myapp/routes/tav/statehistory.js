var express = require('express');
var router = express.Router();


/* GET home page. */
router.get('/tav/statehistory', function(req, res, next) {
  res.render('index', { title: 'StateHistory' });
});

module.exports = router;
